#!/bin/bash

cd ..

git clone https://code.vt.edu/cyber-teachers/tpms.git
git clone https://code.vt.edu/cyber-teachers/kali.git

cd tpms
./get_files.sh
./build_docker.sh

cd ../kali

./build_docker.sh
